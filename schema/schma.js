const graphql = require('graphql');
const lod=require('lodash');
const {
GraphQLObjectType,
GraphQLString,
GraphQLID,
GraphQLSchema,
GraphQLInt,
GraphQLList

}= graphql 

const lessons=[
    {id:"1",name:"react",group:"front",teacherid:"1"},
    {id:"2",name:"angular",group:"front",teacherid:"2"},
    {id:"3",name:"javaee",group:"back",teacherid:"1"},
    {id:"4",name:"django",group:"back",teacherid:"2"},
    {id:"5",name:"vue",group:"front",teacherid:"1"},
]
const teachers=[
    {id:"1",name:"poriyadaliry",age:22},
    {id:"2",name:"omidDvarzani",age:32},
    {id:"3",name:"Mohammdsaliimi",age:12},
]
const Lessontype=new GraphQLObjectType({
    name:"Lesson",
    fields:()=>({
        id:{type:GraphQLID},
        name:{type:GraphQLString},
        group:{type:GraphQLString},
        teacher:{
            type:TeacherType,
            resolve(parent,args){
                return lod.find(teachers,{id:parent.teacherid})
            }
        }


    })
})
const TeacherType=new GraphQLObjectType({
    name:"teacher",
    fields:()=>({
        id:{type:GraphQLID},
        name:{type:GraphQLString},
        age:{type:GraphQLInt},
        lessons:{
            type: new GraphQLList(Lessontype),
            resolve(parent,args){
                return lod.filter(lessons,{teacherid:parent.id})
            }
        }
    })
})

const Root=new GraphQLObjectType({

    name:"rootquery",
    fields:{
        lesson:{
            type:Lessontype,
            args:{id:{type:GraphQLString}},
            resolve(parent,args){
                return lod.find(lessons,{id:args.id})
            }

        },
        teacher:{
            type:TeacherType,
            args:{id:{type:GraphQLID}},
            resolve(parent,args){
                return lod.find(teachers,{id:args.id})
            }
        },
        lessons:{
            type:new GraphQLList(Lessontype) ,
            resolve(){
                return lessons
            }
        },
        teachers:{
            type:GraphQLList(TeacherType),
            resolve(){
                return teachers
            } 
        }
        
    }

});
module.exports=new GraphQLSchema({
    query:Root
})